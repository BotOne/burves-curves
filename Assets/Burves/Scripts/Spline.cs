﻿using UnityEngine;
using System;

namespace Burves
{
    public enum PointMode
    {
        Free,
        Aligned,
        Mirrored
    }

    [SerializeField]
    public class Spline : MonoBehaviour
    {
        private const float DISTANCE_STEP = 0.01f;

        public float length
        {
            get
            {
                return length_;
            }
        }

        public int curveCount
        {
            get
            {
                return (points.Length - 1) / 3;
            }
        }

        public int pointCount
        {
            get
            {
                return points.Length;
            }
        }

        public bool loop
        {
            get
            {
                return loop_;
            }
            set
            {
                loop_ = value;
                if (value == true)
                {
                    modes[modes.Length - 1] = modes[0];
                    SetControlPoint(points[0], 0);
                }
            }
        }

        [SerializeField]
        private float length_;

        [SerializeField]
        private bool loop_;

        [SerializeField]
        private Vector3[] points;

        [SerializeField]
        private PointMode[] modes;

        #region Static Functions

        public static Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float value)
        {
            value = Mathf.Clamp01(value);
            var one = 1.0f - value;
            return Mathf.Pow(one, 3) * p0 +
            3f * Mathf.Pow(one, 2) * value * p1 +
            3f * one * Mathf.Pow(value, 2) * p2 +
            Mathf.Pow(value, 3) * p3;
        }

        public static Vector3 GetDerivative(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float value)
        {
            value = Mathf.Clamp01(value);
            float one = 1.0f - value;
            return 3f * one * one * (p1 - p0) +
            6f * one * value * (p2 - p1) +
            3f * value * value * (p3 - p2);
        }

        #endregion Static Functions

        #region Private Functions

        private void CalculateLength()
        {
            length_ = 0.0f;
            var currPoint = GetPoint(0.0f);
            for (float i = DISTANCE_STEP; i < 1.0f; i += DISTANCE_STEP)
            {
                var nextPoint = GetPoint(i);
                length_ += Vector3.Distance(currPoint, nextPoint);
                currPoint = nextPoint;
            }
        }

        private void EnforcePoint(int index)
        {
            int modeIndex = (index + 1) / 3;
            PointMode mode = modes[modeIndex];
            if (mode == PointMode.Free || !loop_ && (modeIndex == 0 || modeIndex == modes.Length - 1))
                return;

            int middleIndex = modeIndex * 3;
            int fixedIndex, enforcedIndex;
            if (index <= middleIndex)
            {
                fixedIndex = middleIndex - 1;
                if (fixedIndex < 0)
                    fixedIndex = points.Length - 2;
                enforcedIndex = middleIndex + 1;
                if (enforcedIndex >= points.Length)
                    enforcedIndex = 1;
            }
            else
            {
                fixedIndex = middleIndex + 1;
                if (fixedIndex >= points.Length)
                    fixedIndex = 1;
                enforcedIndex = middleIndex - 1;
                if (enforcedIndex < 0)
                    enforcedIndex = points.Length - 2;
            }

            Vector3 middle = points[middleIndex];
            Vector3 enforcedTangent = middle - points[fixedIndex];
            if (mode == PointMode.Aligned)
                enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, points[enforcedIndex]);
            points[enforcedIndex] = middle + enforcedTangent;
        }

        #endregion Private Functions

        #region Public Functions

        public void AddCurve()
        {
            var point = points[points.Length - 1];
            Array.Resize(ref points, points.Length + 3);

            // Add new points.
            point.x += 1f;
            points[points.Length - 3] = point;
            point.x += 1f;
            points[points.Length - 2] = point;
            point.x += 1f;
            points[points.Length - 1] = point;

            // Add new mode.
            Array.Resize(ref modes, modes.Length + 1);
            modes[modes.Length - 1] = modes[modes.Length - 2];

            // Apply mode.
            EnforcePoint(points.Length - 4);

            // Apply loop.
            if (loop_)
            {
                points[points.Length - 1] = points[0];
                modes[modes.Length - 1] = modes[0];
                EnforcePoint(0);
            }

            // Calculate length.
            CalculateLength();
        }

        public void RemoveCurve(int index)
        {
            // Remove from points.
            if (index == 0)
            {
                points = Utilites.RemoveAt(points, 0, 3);
            }
            else if (index == points.Length - 1)
            {
                points = Utilites.RemoveAt(points, points.Length - 3, 3);
            }
            else
            {
                points = Utilites.RemoveAt(points, index - 1, 3);
            }

            // Remove from modes.
            int modeIndex = (index + 1) / 3;
            modes = Utilites.RemoveAt(modes, modeIndex, 1);

            EnforcePoint(0);

            CalculateLength();
        }

        public float GetDistance()
        {
            return 0;
        }

        // Control point mode
        public PointMode GetControlPointMode(int index)
        {
            return modes[(index + 1) / 3];
        }

        public void SetControlPointMode(PointMode mode, int index)
        {
            int modeIndex = (index + 1) / 3;
            modes[modeIndex] = mode;
            if (loop_)
            {
                if (modeIndex == 0)
                    modes[modes.Length - 1] = mode;
                else if (modeIndex == modes.Length - 1)
                    modes[0] = mode;
            }
            EnforcePoint(index);
        }

        // Control point value
        public Vector3 GetControlPoint(int index)
        {
            return points[index];
        }

        public void SetControlPoint(Vector3 value, int index)
        {
            if (index % 3 == 0)
            {
                Vector3 delta = value - points[index];
                if (loop_)
                {
                    if (index == 0)
                    {
                        points[1] += delta;
                        points[points.Length - 2] += delta;
                        points[points.Length - 1] = value;
                    }
                    else if (index == points.Length - 1)
                    {
                        points[0] = value;
                        points[1] += delta;
                        points[index - 1] += delta;
                    }
                    else
                    {
                        points[index - 1] += delta;
                        points[index + 1] += delta;
                    }
                }
                else
                {
                    if (index > 0)
                    {
                        points[index - 1] += delta;
                    }
                    if (index + 1 < points.Length)
                    {
                        points[index + 1] += delta;
                    }
                }
            }

            points[index] = value;
            EnforcePoint(index);

            CalculateLength();
        }

        public Vector3 GetPoint(float value)
        {
            int i;
            if (value >= 1.0f)
            {
                value = 1.0f;
                i = points.Length - 4;
            }
            else
            {
                value = Mathf.Clamp01(value) * curveCount;
                i = (int)value;
                value -= i;
                i *= 3;
            }
            return transform.TransformPoint(GetPoint(points[i + 0], points[i + 1], points[i + 2], points[i + 3], value));
        }

        public Vector3 GetDirection(float value)
        {
            return GetVelocity(value).normalized;
        }

        public Vector3 GetVelocity(float value)
        {
            int i;
            if (value >= 1.0f)
            {
                value = 1f;
                i = points.Length - 4;
            }
            else
            {
                value = Mathf.Clamp01(value) * curveCount;
                i = (int)value;
                value -= i;
                i *= 3;
            }

            return transform.TransformPoint(GetDerivative(points[i + 0], points[i + 1], points[i + 2], points[i + 3], value)) - transform.position;
        }

        #endregion Public Functions

        #region Unity Functions

        private void Reset()
        {
            points = new Vector3[]
            {
                new Vector3(1.0f, 0.0f, 0.0f),
                new Vector3(2.0f, 0.0f, 0.0f),
                new Vector3(3.0f, 0.0f, 0.0f),
                new Vector3(4.0f, 0.0f, 0.0f)
            };

            modes = new PointMode[] {
            PointMode.Free,
            PointMode.Free
        };
        }

        #endregion Unity Functions

        #region Editor Functions

        [ContextMenu("Circle")]
        private void Circle()
        {
            float distance = 0.55191502449f;
            points = new Vector3[]
            {
                new Vector3(0.0f, 1.0f, 0.0f),
                new Vector3(distance, 1.0f, 0.0f),

                new Vector3(1.0f, distance, 0.0f),
                new Vector3(1.0f, 0.0f, 0.0f),
                new Vector3(1.0f, -distance, 0.0f),

                new Vector3(distance, -1.0f, 0.0f),
                new Vector3(0.0f, -1.0f, 0.0f),
                new Vector3(-distance, -1.0f, 0.0f),

                new Vector3(-1.0f, -distance, 0.0f),
                new Vector3(-1.0f, 0.0f, 0.0f),
                new Vector3(-1.0f, distance, 0.0f),

                new Vector3(-distance, 1.0f, 0.0f),
                new Vector3(0.0f, 1.0f, 0.0f)
            };

            modes = new PointMode[]
            {
                PointMode.Mirrored,
                PointMode.Mirrored,
                PointMode.Mirrored,
                PointMode.Mirrored,
                PointMode.Mirrored
            };

            loop = true;
        }

        #endregion Editor Functions
    }
}