﻿using System;
using System.Collections;
using UnityEngine;

namespace Burves
{
    public enum MoveMode
    {
        Once,
        Loop
    }

    [Serializable]
    public class SplineMovePoint
    {
        public float value;
        public float fadeIn;
        public float delay;
        public float fadeOut;
    }

    public class SplineMove : MonoBehaviour
    {
        private const float EPS = 0.0001f;

        private const float SPEED = 0.2f;

        [SerializeField]
        public SplineMovePoint[] points;

        [SerializeField]
        public Spline spline;

        [SerializeField]
        public MoveMode mode;

        [SerializeField]
        public float speed;

        [SerializeField]
        public bool rotate;

        private float progress_;

        private float speed_;

        private bool delay_;

        private bool fade_;

        #region Coroutines

        private IEnumerator PointCoroutine(int index)
        {
            yield return StartCoroutine(FadeCoroutine(points[index].fadeIn, points[index].value, false));
            yield return StartCoroutine(DelayCoroutine(points[index].delay));
            yield return StartCoroutine(FadeCoroutine(points[index].value, points[index].fadeOut, true));
        }

        private IEnumerator FadeCoroutine(float startValue, float endValue, bool inverse = false)
        {
            if (startValue < endValue)
            {
                fade_ = true;
                while (true)
                {
                    var value = 0.0f;
                    if (!inverse)
                        value = 1.0f - (progress_ - startValue) / (endValue - startValue);
                    else
                        value = (progress_ - startValue) / (endValue - startValue);

                    progress_ += speed * Time.deltaTime * Mathf.Clamp(value, SPEED, 1.0f);//Mathf.Lerp(progress_, endValue, value);
                    if (endValue - progress_ <= EPS)
                        break;

                    yield return null;
                }
                fade_ = false;
            }
        }

        private IEnumerator DelayCoroutine(float delay)
        {
            delay_ = true;
            yield return new WaitForSeconds(delay);
            delay_ = false;
        }

        #endregion Coroutines

        #region Private Functions

        private void Delay(int index)
        {
            // Check fadein/fadeout point(if fadein and fadeout not equal value).
            if (progress_ >= points[index].fadeIn && progress_ < points[index].fadeOut)
            {
                StartCoroutine(PointCoroutine(index));
                return;
            }

            // Check next/prev position.
            if (points[index].value > progress_ && points[index].value < progress_ + speed * Time.deltaTime)
            {
                StartCoroutine(PointCoroutine(index));
                return;
            }
        }

        private void Move()
        {
            // Check for fadein/delay/fadeout
            if (!delay_ && !fade_)
            {
                for (int i = 0; i < points.Length; ++i)
                    Delay(i);
            }

            // Move.
            if (progress_ >= 1)
            {
                switch (mode)
                {
                    case MoveMode.Once:
                        progress_ = 1.0f;
                        break;

                    case MoveMode.Loop:
                        progress_ = 0.0f;
                        break;
                }
            }

            // Update position.
            var position = spline.GetPoint(progress_);
            transform.localPosition = position;

            // Update rotation.
            if (rotate)
                transform.LookAt(position + spline.GetDirection(progress_));

            if (!delay_ && !fade_)
                progress_ += speed * Time.deltaTime;
        }

        #endregion Private Functions

        #region Public Functions

        public void AddPoint()
        {
            if (points == null)
                points = new SplineMovePoint[0];

            var value = points.Length == 0 ? 0.0f : points[points.Length - 1].value + ((1.0f - points[points.Length - 1].value) * 0.5f);
            Array.Resize(ref points, points.Length + 1);
            points[points.Length - 1] = new SplineMovePoint();
            points[points.Length - 1].value = value;
            points[points.Length - 1].fadeIn = value;
            points[points.Length - 1].fadeOut = value;
        }

        public void RemovePoint(int index)
        {
            points = Utilites.RemoveAt(points, index, 1);
        }

        #endregion Public Functions

        #region Unity Functions

        private void Update()
        {
            Move();
        }

        #endregion Unity Functions
    }
}