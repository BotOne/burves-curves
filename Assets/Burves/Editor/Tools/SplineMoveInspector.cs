﻿using UnityEngine;
using UnityEditor;

namespace Burves
{
    [CustomEditor(typeof(SplineMove))]
    public class SplineMoveInspector : Editor
    {
        private const float STEP = 0.001f;

        private SplineMove splineMove;
        private Transform handleTransform;
        private Quaternion handleRotation;

        private int index_;
        private int tab_;

        #region Override Functions

        public override void OnInspectorGUI()
        {
            //base.OnInspectorGUI();

            splineMove = (SplineMove)target;

            splineMove.spline = (Spline)EditorGUILayout.ObjectField("Spline", splineMove.spline, typeof(Spline), true);

            // Check script.
            if (splineMove.spline == null)
                return;

            // Tabs.
            tab_ = GUILayout.Toolbar(tab_, new string[] { "Attributes", "Points" });

            switch (tab_)
            {
                case 0:
                    DrawAttributes();
                    break;

                case 1:
                    DrawPoints();
                    break;
            }
        }

        #endregion Override Functions

        #region Private Functions

        private void DrawAttributes()
        {
            GUILayout.BeginVertical(EditorStyles.helpBox);

            // Speed.
            EditorGUI.BeginChangeCheck();
            float speed = EditorGUILayout.FloatField("Speed", splineMove.speed);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(splineMove, "Change Speed");
                splineMove.speed = speed;
                EditorUtility.SetDirty(splineMove);
            }

            // Mode.
            EditorGUI.BeginChangeCheck();
            MoveMode mode = (MoveMode)EditorGUILayout.EnumPopup("Mode", splineMove.mode);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(splineMove, "Change Mode");
                EditorUtility.SetDirty(splineMove);
                splineMove.mode = mode;
            }

            // Rotation.
            EditorGUI.BeginChangeCheck();
            bool rotation = EditorGUILayout.Toggle("Rotate", splineMove.rotate);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(splineMove, "Change Rotate Mode");
                EditorUtility.SetDirty(splineMove);
                splineMove.rotate = rotation;
            }

            GUILayout.EndVertical();
        }

        private void DrawPoints()
        {
            if (splineMove.points != null)
            {
                GUILayout.BeginVertical(EditorStyles.helpBox);

                for (int i = 0; i < splineMove.points.Length; ++i)
                    DrawPoint(i);

                GUILayout.EndVertical();
            }

            if (GUILayout.Button("Add Point"))
            {
                Undo.RecordObject(splineMove, "Add Point");
                splineMove.AddPoint();
                UpdatePoint(splineMove.points.Length - 1);
                EditorUtility.SetDirty(splineMove);
            }
        }

        private void DrawPoint(int index)
        {
            GUILayout.BeginHorizontal(EditorStyles.helpBox);

            // Fold.
            if (GUILayout.Button("Point", EditorStyles.toolbarButton))
            {
                if (index == index_)
                    index_ = -1;
                else
                    index_ = index;

                EditorUtility.SetDirty(splineMove);
            }

            GUILayout.FlexibleSpace();

            // Remove button.
            if (GUILayout.Button("-", GUILayout.Width(25)))
            {
                Undo.RecordObject(splineMove, "Remove Point");
                splineMove.RemovePoint(index);
                EditorUtility.SetDirty(splineMove);
                index_ = -1;
            }

            GUILayout.EndHorizontal();

            // Check visibility.
            var visible = index == index_;
            if (!visible)
                return;

            // Delay.
            EditorGUI.BeginChangeCheck();
            float delay = EditorGUILayout.FloatField("Delay: ", splineMove.points[index].delay);
            if (delay < 0)
                delay = 0.0f;
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(splineMove, "Change Point Delay");
                splineMove.points[index].delay = delay;
                EditorUtility.SetDirty(splineMove);
            }

            // Value.
            EditorGUI.BeginChangeCheck();
            float value = EditorGUILayout.Slider("Value: ", splineMove.points[index].value, 0.0f, 1.0f);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(splineMove, "Change Point Value");
                splineMove.points[index].value = value;
                UpdatePoint(index);
                EditorUtility.SetDirty(splineMove);
            }

            // Fade in.
            EditorGUI.BeginChangeCheck();
            float fadeIn = EditorGUILayout.Slider("Fade In: ", splineMove.points[index].fadeIn, 0.0f, 1.0f);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(splineMove, "Change Point FadeIn");
                splineMove.points[index].fadeIn = fadeIn;
                UpdatePoint(index);
                EditorUtility.SetDirty(splineMove);
            }
            //EditorGUI.BeginChangeCheck();
            //float fadeIn = EditorGUILayout.Slider("Fade In: ", splineMove.points[index].fadeIn, 0.0f, 1.0f);
            //if (EditorGUI.EndChangeCheck())
            //{
            //    Undo.RecordObject(splineMove, "Change Point FadeIn");
            //    splineMove.points[index].fadeIn = fadeIn;
            //    UpdatePoint(index);
            //    EditorUtility.SetDirty(splineMove);
            //}

            // Fade out.
            EditorGUI.BeginChangeCheck();
            float fadeOut = EditorGUILayout.Slider("Fade Out: ", splineMove.points[index].fadeOut, 0.0f, 1.0f);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(splineMove, "Change Point FadeOut");
                splineMove.points[index].fadeOut = fadeOut;
                UpdatePoint(index);
                EditorUtility.SetDirty(splineMove);
            }
        }

        private void UpdatePoint(int index)
        {
            var minValue = index == 0 ? 0.0f : splineMove.points[index - 1].fadeOut;
            var maxValue = index == splineMove.points.Length - 1 ? 1.0f : splineMove.points[index + 1].fadeIn;

            splineMove.points[index].value = Mathf.Clamp(splineMove.points[index].value, minValue, maxValue);
            splineMove.points[index].fadeIn = Mathf.Clamp(splineMove.points[index].fadeIn, minValue, splineMove.points[index].value);
            splineMove.points[index].fadeOut = Mathf.Clamp(splineMove.points[index].fadeOut, splineMove.points[index].value, maxValue);
        }

        #endregion Private Functions

        #region Editor Functions

        private void OnSceneGUI()
        {
            splineMove = (SplineMove)target;
            if (splineMove.spline == null)
                return;

            handleTransform = splineMove.spline.transform;

            // Draw path.
            Handles.color = Color.gray;
            var point0 = handleTransform.TransformPoint(splineMove.spline.GetControlPoint(0));
            for (int i = 1; i < splineMove.spline.pointCount; i += 3)
            {
                var point1 = handleTransform.TransformPoint(splineMove.spline.GetControlPoint(i + 0));
                var point2 = handleTransform.TransformPoint(splineMove.spline.GetControlPoint(i + 1));
                var point3 = handleTransform.TransformPoint(splineMove.spline.GetControlPoint(i + 2));

                Handles.DrawBezier(point0, point3, point1, point2, Color.white, null, 2.0f);

                point0 = point3;
            }

            // Draw points.
            if (splineMove.points != null)
            {
                for (int i = 0; i < splineMove.points.Length; ++i)
                {
                    var position = handleTransform.TransformPoint(splineMove.spline.GetPoint(splineMove.points[i].value));

                    // Get attributes.
                    float size = HandleUtility.GetHandleSize(position) * 0.05f;
                    float value = splineMove.points[i].value;

                    // Draw fadeIn.
                    float fadeIn = splineMove.points[i].fadeIn;
                    if (fadeIn != value)
                    {
                        Handles.color = Color.green;
                        var fadePosition = handleTransform.TransformPoint(splineMove.spline.GetPoint(fadeIn));
                        Handles.DotHandleCap(0, fadePosition, Quaternion.identity, size * 0.5f, EventType.Repaint);
                        for (fadeIn += STEP; fadeIn < value; fadeIn += STEP)
                        {
                            var nextFadePosition = handleTransform.TransformPoint(splineMove.spline.GetPoint(fadeIn));
                            Handles.DrawLine(fadePosition, nextFadePosition);
                            fadePosition = nextFadePosition;
                        }
                    }

                    // Draw fadeOut.
                    float fadeOut = splineMove.points[i].fadeOut;
                    if (fadeOut != value)
                    {
                        Handles.color = Color.red;
                        var fadeOutPosition = handleTransform.TransformPoint(splineMove.spline.GetPoint(fadeOut));
                        Handles.DotHandleCap(0, fadeOutPosition, Quaternion.identity, size * 0.5f, EventType.Repaint);
                        for (fadeOut -= STEP; fadeOut > value; fadeOut -= STEP)
                        {
                            var nextFadePosition = handleTransform.TransformPoint(splineMove.spline.GetPoint(fadeOut));
                            Handles.DrawLine(fadeOutPosition, nextFadePosition);
                            fadeOutPosition = nextFadePosition;
                        }
                    }

                    // Draw point.
                    Handles.color = i == index_ ? Color.green : Color.white;
                    Handles.DotHandleCap(0, position, Quaternion.identity, size, EventType.Repaint);
                }
            }
        }

        #endregion Editor Functions
    }
}