﻿using UnityEditor;
using UnityEngine;

namespace Burves
{
    [CustomEditor(typeof(Spline))]
    public class SplineInspector : Editor
    {
        private readonly static Color[] colors =
        {
            Color.white,
            Color.yellow,
            Color.cyan
        };

        private const int STEPS = 10;
        private const float SIZE = 2.5f;
        private const float SCALE = 0.25f;
        private const float HANDLE_SIZE = 0.05f;
        private const float HANDLE_PICK = 0.1f;

        private Spline spline;
        private Transform handleTransform;
        private Quaternion handleRotation;
        private int selectedIndex = -1;

        #region Override Functions

        public override void OnInspectorGUI()
        {
            spline = (Spline)target;

            EditorGUI.BeginChangeCheck();
            bool loop = EditorGUILayout.Toggle("Loop", spline.loop);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Toggle Loop");
                EditorUtility.SetDirty(spline);
                spline.loop = loop;
            }

            // Information
            GUILayout.BeginHorizontal(EditorStyles.helpBox);
            GUILayout.Label("Spline length: " + spline.length);
            GUILayout.EndHorizontal();

            if (selectedIndex >= 0 && selectedIndex < spline.pointCount)
                ShowSelecterPoint();

            // Buttons
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add Curve"))
            {
                Undo.RecordObject(spline, "Add Curve");
                spline.AddCurve();
                EditorUtility.SetDirty(spline);
            }

            if (spline.pointCount > 4 && selectedIndex % 3 == 0)
            {
                if (GUILayout.Button("Remove Curve"))
                {
                    Undo.RecordObject(spline, "Remove Curve");
                    spline.RemoveCurve(selectedIndex);
                    EditorUtility.SetDirty(spline);
                    selectedIndex = -1;
                }
            }
            GUILayout.EndHorizontal();
        }

        #endregion Override Functions

        #region Private Functions

        private Vector3 ShowPoint(int index)
        {
            Vector3 point = handleTransform.TransformPoint(spline.GetControlPoint(index));
            float size = HandleUtility.GetHandleSize(point);
            Handles.color = colors[(int)spline.GetControlPointMode(index)];

            // Apply main points attributes.
            if (index % 3 == 0)
                size *= 2.0f;

            // Draw interactive point.
            if (Handles.Button(point, handleRotation, size * HANDLE_SIZE, size * HANDLE_PICK, Handles.RectangleHandleCap)) //Handles.SphereHandleCap
            {
                selectedIndex = index;
                Repaint();
            }

            // Draw selected point.
            if (selectedIndex == index)
            {
                EditorGUI.BeginChangeCheck();
                point = Handles.DoPositionHandle(point, handleRotation);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(spline, "Move Point");
                    EditorUtility.SetDirty(spline);
                    spline.SetControlPoint(handleTransform.InverseTransformPoint(point), index);
                }
            }

            return point;
        }

        private void ShowDirections()
        {
            Handles.color = Color.green;
            Vector3 point = spline.GetPoint(0f);
            Handles.DrawLine(point, point + spline.GetDirection(0f) * SCALE);
            int steps = STEPS * spline.curveCount;
            for (int i = 0; i < steps; ++i)
            {
                point = spline.GetPoint(i / (float)steps);
                Handles.DrawLine(point, point + spline.GetDirection(i / (float)steps) * SCALE);
            }
        }

        private void ShowSelecterPoint()
        {
            GUILayout.BeginVertical(EditorStyles.helpBox);

            GUILayout.Label("Selected Point: " + selectedIndex);

            // Point position
            EditorGUI.BeginChangeCheck();
            Vector3 point = EditorGUILayout.Vector3Field("Position", spline.GetControlPoint(selectedIndex));
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Move Point");
                EditorUtility.SetDirty(spline);
                spline.SetControlPoint(point, selectedIndex);
            }

            // Point mode
            EditorGUI.BeginChangeCheck();
            PointMode mode = (PointMode)EditorGUILayout.EnumPopup("Mode", spline.GetControlPointMode(selectedIndex));
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Change Point Mode");
                spline.SetControlPointMode(mode, selectedIndex);
                EditorUtility.SetDirty(spline);
            }

            GUILayout.EndVertical();
        }

        #endregion Private Functions

        #region Editor Functions

        private void OnSceneGUI()
        {
            spline = (Spline)target;
            handleTransform = spline.transform;
            handleRotation = Tools.pivotRotation == PivotRotation.Local ? handleTransform.rotation : Quaternion.identity;

            Tools.current = Tool.None;

            var point0 = ShowPoint(0);
            for (int i = 1; i < spline.pointCount; i += 3)
            {
                var point1 = ShowPoint(i + 0);
                var point2 = ShowPoint(i + 1);
                var point3 = ShowPoint(i + 2);

                Handles.color = Color.gray;
                Handles.DrawDottedLine(point0, point1, SIZE);
                Handles.DrawDottedLine(point2, point3, SIZE);

                Handles.DrawBezier(point0, point3, point1, point2, Color.white, null, 2.0f);

                point0 = point3;
            }
        }

        #endregion Editor Functions
    }
}